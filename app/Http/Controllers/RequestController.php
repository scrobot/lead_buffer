<?php

namespace App\Http\Controllers;

use App\Mix\AmoManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mockery\Exception;

use App\Models\Lead;
use App\Models\Manager;

class RequestController extends Controller
{

    protected $amoManager;

    public function __construct(AmoManager $amoManager)
    {
        $this->middleware('api');
        $this->amo = $amoManager;
    }

    public function postLead(Request $request)
    {
        $headers = $request->headers->all();
        $input = $request->all();
        $serialized = serialize($input['ser']);

        $lead = Lead::create([
            'host' => $headers['host'][0],
            'url' => $request->input('url'),
            'label' => 'xxx',
            'fields' => $serialized,
            'validation_status' => 0,
            'amocrm_task_id' => null,
            'created_at' => time()
        ]);

        if($lead->validation()) {
            $lead->submitLead($this->amo);
        }

        return response(['status' => 200]);
    }

    public function postUpdateManagers()
    {
        $managers = $this->amo->getUsers();

        foreach($managers as $getting_manager) {

            $manager_array = [
                'amo_manager_id' => $getting_manager['id'],
                'name' => $getting_manager['name'],
                'lats_name' => $getting_manager['last_name'],
                'login' => $getting_manager['login'],
            ];

            $manager = Manager::where('amo_manager_id', $manager_array['amo_manager_id'])->first();

            if(is_null($manager)) {
                Manager::create($manager_array);
            } else {
                $manager->update($manager_array);
            }
        }
    }

}
