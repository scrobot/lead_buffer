<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \LeadBuffer\Api;

use App\Mix\AmoManager;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Lead;
use App\Models\Manager;

class TestController extends Controller
{

    protected $amoManager;

    public function __construct(AmoManager $amoManager)
    {
        $this->amo = $amoManager;
    }

    public function getTest0()
    {
        $api = new Api('I470egPqrLcO353cZem0qBT', 'http://lead-buffer.five-dots.ru', 'http://lead-buffer.five-dots.ru/test/test1');
        $r = $api->updateManagers();

        dd($r);
    }

    public function getTest1()
    {
        $api = new Api('I470egPqrLcO353cZem0qBT', 'http://lead-buffer.five-dots.ru', 'http://lead-buffer.five-dots.ru/test/test1');
        $result = $api->createLead([
            'name' => 'Vasya',
            'email' => 'VasyaPirogov@behance.com',
            'phone' => '89253213211',
            'message' => 'klaskdaspklfapf akpjfdnvzchv ankdhjvqok3eh1fc'
        ]);

        var_dump($result);
    }

    public function getTestRecord(Request $request)
    {

        $headers = $request->headers->all();
        $lead = Lead::create([
            'host' => $headers['host'][0],
            'url' => $request->url(),
            'label' => 'xxx',
            'fields' => 'a:3:{s:4:"name";s:5:"Vasya";s:5:"email";s:24:"VasyaPirogov@behance.com";s:5:"phone";s:11:"89253213211";}',
            'validation_status' => 0,
            'amocrm_task_id' => null,
            'created_at' => time()
        ]);

        if($lead->validation()) {
            $lead->submitLead($this->amo);
        }
    }

    public function getAmoUsers()
    {

        $managers = $this->amo->getUsers();

        foreach($managers as $getting_manager) {

            $manager_array = [
                'amo_manager_id' => $getting_manager['id'],
                'name' => $getting_manager['name'],
                'lats_name' => $getting_manager['last_name'],
                'login' => $getting_manager['login'],
            ];

            $manager = Manager::where('amo_manager_id', $manager_array['amo_manager_id'])->first();

            if(is_null($manager)) {
                Manager::create($manager_array);
            } else {
                $manager->update($manager_array);
            }
        }

        return 'Managers updated!';
    }

    public function getMaxAmoUsers()
    {
        $manager = $this->amo->getUsers();
        dd($manager);
    }

    public function getCreateLead(){

        $lead = Lead::find(17);
        $lead->submitLead($this->amo);

    }

    public function getAccountInfo(){
        dd($this->amo->getAccountInfo());
    }

    public function getLeadsList()
    {
        dd($this->amo->getLeads());
    }

    public function getTasksList()
    {
        dd($this->amo->getTasks());
    }

    public function getNotes()
    {
        dd($this->amo->getNotes());
    }

}
