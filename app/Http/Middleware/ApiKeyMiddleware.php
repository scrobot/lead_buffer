<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_key = config('app.api_key');
        if($request->input('api_key') == $api_key) {
            \Log::info('Api_key_right',['message' => 'right api_key']);
            return $next($request);
        } else {
            \Log::info('Api_key_wrong',['message' => 'wrong api_key']);
        }
    }
}
