<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manager;

class Lead extends Model {

    protected $table = 'leeds';

    protected $guarded = [];

    public $timestamps = false;

    public function validation()
    {
        $fields = unserialize($this->fields);

        $validator = \Validator::make(
            [
                'name' => $fields['name'],
                'phone' => $fields['phone'],
                'email' => $fields['email']
            ],
            [
                'name' => 'min:3',
                'phone' => 'numeric',
                'email' => 'email'
            ]
        );

        if($validator->fails()) {
            return false;
        } else {
            $this->validation_status = 1;
            $this->save();
        }

        return true;

    }

    public function submitLead($amo)
    {
        $manager = Manager::getNextManager();
        $lead = $amo->createLead("Новая заявка с сайта {$this->url}", env('AMOCRM_STATUS_ID'), null, $manager->amo_manager_id);
        $manager->leads_limit -= 1;
        $manager->save();

        $this->submitNote($lead, $amo, $manager->amo_manager_id);

        return true;
    }

    public function submitNote($lead_id, $amo, $manager_id)
    {
        $fields = unserialize($this->fields);
        $text = "Имя: {$fields['name']}. E-mail: {$fields['email']}. Телефон: {$fields['phone']}. ";
        if(isset($fields['message'])) {
            $text .= "Сообщение: {$fields['message']}";
        }
        $amo->createNote($lead_id, $text, $manager_id);

        return true;
    }

}