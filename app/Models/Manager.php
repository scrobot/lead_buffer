<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model {

    protected $table = 'managers';

    protected $guarded = [];

    static private function getMinLeadTime()
    {
        return \DB::table('managers')->where('leads_limit', '!=', 0)->min('updated_at');
    }

    static public function getNextManager()
    {
        $time = self::getMinLeadTime();
        $manager = self::where('updated_at', $time)->first();
        return $manager;
    }

}