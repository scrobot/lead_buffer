<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mix\AmoManager;


class AmoManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton('App\Mix\AmoManager', function($app)
        {
            return new AmoManager($app['config']['app']['amocrm']);
        });

    }
}
