<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Manager;
use App\Mix\AmoManager;

class ManagersList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amocrm:managers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating or Create Managers in table `managers`.';


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param AmoManager $amoManager
     * @return mixed
     */
    public function handle()
    {

        $amo = new AmoManager([
            'subdomain' => config('app.amocrm.subdomain'),
            'login' => config('app.amocrm.login'),
            'key' => config('app.amocrm.key')
        ]);
        $managers = $amo->getUsers();

        foreach($managers as $getting_manager) {

            $manager_array = [
                'amo_manager_id' => $getting_manager['id'],
                'name' => $getting_manager['name'],
                'lats_name' => $getting_manager['last_name'],
                'login' => $getting_manager['login'],
            ];

            $manager = Manager::where('amo_manager_id', $manager_array['amo_manager_id'])->first();

            if(is_null($manager)) {
                Manager::create($manager_array);
            } else {
                $manager->update($manager_array);
            }
        }

        $this->info('Managers updated!');
    }
}
