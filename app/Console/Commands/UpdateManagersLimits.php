<?php

namespace App\Console\Commands;

use App\Mix\AmoManager;
use Illuminate\Console\Command;

class UpdateManagersLimits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amocrm:managers-limits-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating limits of leads in managers table';

    /**
     * @param AmoManager $amoManager
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('managers')->where('id', '>', 0)->update([
            'leads_limit' => config('app.limit')
        ]);

        $this->info('Managers Limit updated');
    }
}
