<?php

namespace App\Mix;


class AmoManager {
    private $config;

    private $api;

    public function __construct($config)
    {
        $this->config = $config;
    }

    private function initConnection()
    {

        if (! $this->api) {
            $this->api = new \AmoRestApi($this->config['subdomain'], $this->config['login'], $this->config['key']);
        }
    }

    public function createTask($lead_id, $text, $responsible_manager_id)
    {
        $this->initConnection();
        $this->api->setTasks(array('add' => array(
            array(
                "element_id" =>  $lead_id,
                "element_type" => 2,
                "task_type" =>  1,
                "text" =>  $text,
                "complete_till" =>  time() + 60 * 60,
                'responsible_user_id' => $responsible_manager_id
            )
        )));
    }

    /**
     * @return integer id
     */
    public function createLead($name, $status_id, $text, $responsible_id)
    {
        $this->initConnection();
        $leads_to_add = array(
            array(
                'name' => $name,
                'status_id' => $status_id,
                // 'price' => '',
                'responsible_user_id' => $responsible_id,
                'tags' => 'заявка с сайта,',
                'custom_fields' => array(
                    array(
                        'id' => $this->config['field_ids']['info'],
                        'values' => array(
                            array(
                                'value' => $text
                            ),
                        )
                    )
                )
            ));
        $res = $this->api->setLeads(array('add' => $leads_to_add));
        return current($res)['id'];
    }

    public function createContact($name, $phone, $email, array $lead_ids, $responsible_user_id)
    {
        $this->initConnection();
        return $this->api->setContacts(array('add' => array(
            'name' => $name,
            'linked_leads_id' => $lead_ids,
            'responsible_user_id' => $responsible_user_id,
            'custom_fields' => array(
                array(
                    'id' => $this->config['field_ids']['phone'],
                    'values' => array(
                        array(
                            'value' => $phone,
                            'enum' => 'WORK'
                        )
                    )
                ),
                array(
                    'id' => $this->config['field_ids']['email'],
                    'values' => array(
                        array(
                            'enum' => 'WORK',
                            'value' => $email
                        )
                    )
                )
            )
        )));
    }

    public function createNote($lead_id, $text, $responsible_id)
    {
        $this->initConnection();
        return $this->api->setNotes([
            'add' => [
                [
                    'element_id'=>$lead_id,
                    'element_type'=>2,
                    'note_type'=>4,
                    'text'=>$text,
                    'responsible_user_id'=>$responsible_id,
                ]
            ]
        ]);
    }


    public function getAccountInfo()
    {
        $this->initConnection();
        static $accountInfo = null;
        if (! $accountInfo) {
            $accountInfo = $this->api->getAccountInfo();
        }
        return $accountInfo;
    }

    public function getUsers()
    {
        $this->initConnection();
        $accountInfo = $this->getAccountInfo();
        return $accountInfo['users'];
    }

    public function getLeads() {
        $this->initConnection();
        return $this->api->getLeadsList();
    }

    public function getTasks() {
        $this->initConnection();
        return $this->api->getTasksList();
    }

    public function getNotes() {
        $this->initConnection();
        return $this->api->getNotesList();
    }
}